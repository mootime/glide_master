﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishLine : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter (Collider collide){

    if(collide.tag == "Player"){

      GameObject.Find("Character").GetComponent<CharecterController>().enabled = false;
      GameObject.Find("Character/MenuManager/ EndScreenManager/EndScreen").SetActive(true);
      GameObject chute = GameObject.Instantiate(Resources.Load("prefab/char_rot"), GameObject.Find("Character/MainCharacter_Low").transform.position, transform.rotation) as GameObject;
      chute.transform.parent = GameObject.Find("Character").transform;
      //chute.transform.localPosition.y = -0.83;
      GameObject.Find("Character/MainCharacter_Low").SetActive(false);
    }

    }


}
