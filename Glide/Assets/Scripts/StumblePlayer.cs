﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StumblePlayer : MonoBehaviour
{

    public PointsSystem PointsSystem_script;
    public bool killsplayer = false;


    //MAKE SURE the "Trigger" Object is the first child of charecter
    void OnTriggerEnter (Collider collide){

    if(collide.gameObject.tag == "Player" && killsplayer == false){
        //collide.gameObject.transform.GetChild(0).GetComponent<PointsSystem>();
        PointsSystem_script = collide.GetComponentInChildren<PointsSystem>();
        PointsSystem_script.hit_object = true;  
        PointsSystem_script.Invoke("HitCooldown", 1.0f); 
    }

    if(collide.gameObject.tag == "Player" && killsplayer == true){
        collide.GetComponent<CharecterController>().KillPlayer();
    } 

    }

}
