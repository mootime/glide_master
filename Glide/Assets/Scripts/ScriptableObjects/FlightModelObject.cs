﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Glide
{
    [CreateAssetMenu(fileName = "FlightModelObj", menuName = "ScriptableObjects/FlightModel", order = 1)]
    public class FlightModelObject : ScriptableObject
    {
        [Tooltip("Applied force to re-orient player to a level or 'ideal' flight pattern")]
        public AnimationCurve RollCorrectionCurve;

        
        
        
        public AnimationCurve LateralSpeedToYawCorrection;

        public AnimationCurve PitchInputAttackRamp;
        public AnimationCurve RollInputAttackRamp;


        [Tooltip("Where in the pitch should there be roll correction")]
        public AnimationCurve PitchToRollCorrectionRamp;


        [Tooltip("Where in the pitch to apply a speed increase forward")]
        public AnimationCurve PitchToVelocityIncrease;
        [Tooltip("How Much drag is applied based on pitch")]
        public AnimationCurve PitchToDragIncrease;

        [Tooltip("How much pitch contributes to lift")]
        public AnimationCurve PitchToGlideLift;

        [Tooltip("Terminal Velocity")]
        public float MaxGlideSpeed;
        public float MinGlideSpeed;

        [Tooltip("how much force to apply with pitched down")]
        public float MaxGlideForce;

        [Tooltip("Rate modifier for glide accelaration/deccelleration")]
        public float GlideChangeRate;

        public float BankFromRollAmount;
        public float PitchInputModifier;
        public float RollInputModifier;

        public float GlideLiftModifier;

        [Tooltip("What is the ideal pitch for gliding forward")]
        public float LiftAngle;

        public float MaxBodyDrag;

        public float YawCorrectionRate;
        public float RollCorrectionRate;
    }

}
