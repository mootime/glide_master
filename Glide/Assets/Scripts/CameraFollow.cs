﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    public Transform player; 
    public Vector3 offset;


    // Start is called before the first frame update
    void Start()
    {

        player = GameObject.Find("Charecter").transform;
        offset = transform.position - player.position;  
          
    }

    // Update is called once per frame
    void Update()
    {

      Vector3 pos = player.position;
      transform.position = pos + offset;  
              
    }
}
