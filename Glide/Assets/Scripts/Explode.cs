﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explode : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        
    }

  void OnCollisionEnter (Collision collide){

            Instantiate(Resources.Load("prefab/Particle_Explosion"), transform.position, transform.rotation);
            Instantiate(Resources.Load("prefab/FX_AudioShot"), transform.position, transform.rotation);
            Destroy(this.gameObject);

  }

}
