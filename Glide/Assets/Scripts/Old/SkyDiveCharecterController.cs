﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyDiveCharecterController : MonoBehaviour
{

    public float CurrentSpeed = 0;

    public float MaxSpeed = 30;

    public float LocalSpeed = 5;

    public Vector3 StartVector;

    public Vector3 EndVector;

    private CharacterController controller;

    private Animator anim_controller;

    // Start is called before the first frame update
    void Start()
    {

        controller = GetComponent<CharacterController>();
        anim_controller = GetComponent<Animator>();
        StartVector = GameObject.Find("Start_Position").transform.position;
        EndVector = GameObject.Find("End_Position").transform.position;
             
    }

    // Update is called once per frame
    void Update()
    {

    //move player forward between start and end

    controller.Move(Vector3.Lerp (StartVector, EndVector, Mathf.PingPong(Time.time*CurrentSpeed, 1.0f)));

    if(Input.GetKey(KeyCode.W)){

      controller.Move(transform.TransformDirection(Vector3.up) * Time.deltaTime * LocalSpeed);

    }

    if(Input.GetKey(KeyCode.S)){

      controller.Move(transform.TransformDirection(Vector3.down) * Time.deltaTime * LocalSpeed);

    }

    if(Input.GetKey(KeyCode.A)){

      controller.Move(transform.TransformDirection(Vector3.left) * Time.deltaTime * LocalSpeed);

    }

    if(Input.GetKey(KeyCode.D)){

      controller.Move(transform.TransformDirection(Vector3.right) * Time.deltaTime * LocalSpeed);

    }

    //controller.Move();
           
    }
}
