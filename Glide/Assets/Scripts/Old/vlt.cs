﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vlt : MonoBehaviour
{

    public Vector3 v1;
    public Vector3 v2;

    public float startTime;

    public float jl;

    // Start is called before the first frame update
    void Start()
    {

    startTime = Time.time; 
    jl = Vector3.Distance(v1, v2);

    }

    // Update is called once per frame
    void Update()
    {

    float curdist = (Time.time - startTime) * 1.0f;

    float frac = curdist / jl;

    v1 = Vector3.Lerp(v1,v2, frac);
          
    }
}
