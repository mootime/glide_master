﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioShot : MonoBehaviour
{

    public float destroytime = 5;
    private float startime = 0;
    AudioSource muPlayer;
    public string target = "FX/Big_Bomb";

    // Start is called before the first frame update
    void Start()
    {

    startime = Time.time;
    muPlayer = GetComponent<AudioSource>(); 
    muPlayer.clip = Resources.Load<AudioClip>(target);
    muPlayer.PlayOneShot(Resources.Load<AudioClip>(target), 0.2f);
    }

    // Update is called once per frame
    void Update()
    {

        if(Time.time > (startime + destroytime)){

            Destroy(this.gameObject);

        }

    }
}
