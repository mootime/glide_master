﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Glide
{
    public class BasicFlightController : MonoBehaviour
    {

        #region Variables

        public FlightModelObject myFlightModel;
        public float BaseSpeed = 2;
        public float StartKick = 500;


        [SerializeField]
        private Rigidbody myRB;

        private float lastRollInputTime;
        private float lastPitchInputTime;
        private float lastRollIdleTime;
        private float lastPitchIdleTime;

        private float glideForceForward;
        private float glideLiftForce;


        #endregion

        #region Properties 

        public Vector3 HeadsUpForward { get { return myRB.transform.forward; } }

        Vector3 LiftDirection { get { return Quaternion.AngleAxis(myFlightModel.LiftAngle, myRB.transform.right) * myRB.transform.forward * -1; } }
        Vector3 MaxGlideLift { get { return Physics.gravity.magnitude * LiftDirection * myRB.mass * myFlightModel.GlideLiftModifier; } }
        Vector3 StandardExtremeAngleDragForce { get { return Physics.gravity.magnitude * LiftDirection * myRB.mass; } }

        public float CurrentRollAmount { get { return Vector3.Dot(myRB.transform.right, Vector3.up) * -1f; } }
        public bool UpsideDown { get { return Vector3.Dot(Physics.gravity.normalized, myRB.transform.forward) < 0.0f; } }

        public float PitchToRawGlideForce { get { return myFlightModel.PitchToVelocityIncrease.Evaluate(PitchAmountRelativeToGravity); } }
        public float RawDragFromPitch { get { return myFlightModel.PitchToDragIncrease.Evaluate(PitchAmountRelativeToGravity); } }

        public float NormalizedPitchAmount
        {
            get
            {
                if (Physics.gravity.normalized == Vector3.zero) { return 0.0f; }
                return Mathf.Abs(Vector3.Dot(HeadsUpForward, Physics.gravity.normalized));
            }
        }
        public float AngleOfAttack
        {
            get
            {
                if (myRB.velocity.normalized == Vector3.zero) { return 0.0f; }
                float angleOfAttackToGravityDot = Vector3.Dot(HeadsUpForward, myRB.velocity.normalized);
                angleOfAttackToGravityDot = Mathf.Clamp(angleOfAttackToGravityDot, -1.0f, 1.0f);
                return Mathf.Acos(angleOfAttackToGravityDot) * Mathf.Rad2Deg;
            }
        }
        public float PitchAmountRelativeToGravity
        {
            get
            {
                if (Physics.gravity.normalized == Vector3.zero) { return 0.0f; }
                float pitchToGravityDot = Vector3.Dot(HeadsUpForward, Physics.gravity.normalized * -1);
                pitchToGravityDot = Mathf.Clamp(pitchToGravityDot, -1.0f, 1.0f);
                return Mathf.Asin(pitchToGravityDot) * Mathf.Rad2Deg;
            }
        }

        #endregion



        // Start is called before the first frame update
        void Start()
        {
            if (myRB == null) GetComponent<Rigidbody>();

            //myRB.AddForce(myRB.transform.forward * StartKick);
        }

        private void InputUpdateRotations()
        {
            float pitchInput = Input.GetAxis("Mouse Y") * myFlightModel.PitchInputModifier * Time.deltaTime;
            float rollInput = -Input.GetAxis("Mouse X") * myFlightModel.RollInputModifier * Time.deltaTime;
            //Debug.Log($"{pitchInput.ToString()}--pitchInput {rollInput.ToString()}--rollINput");

            float attackRampPitchModifier = myFlightModel.PitchInputAttackRamp.Evaluate(Time.time - lastPitchIdleTime);
            float attackRampRollModifier = myFlightModel.RollInputAttackRamp.Evaluate(Time.time - lastRollIdleTime);


            myRB.transform.Rotate(Vector3.right, pitchInput * attackRampPitchModifier, Space.Self);
            myRB.transform.Rotate(Vector3.forward, rollInput * attackRampRollModifier, Space.Self);

            if (pitchInput!= 0) { lastPitchInputTime = Time.time; } else { lastPitchIdleTime = Time.time; }
            if (rollInput != 0){ lastRollInputTime = Time.time; } else { lastRollIdleTime = Time.time; }
        }

        private void UpdateIdealRollRotation()
        {
            Quaternion idealRoll = Quaternion.FromToRotation(myRB.transform.up, Vector3.up); //Quaternion.LookRotation(myRB.transform.up, Vector3.up);
            idealRoll = Quaternion.AngleAxis(90f, idealRoll * Vector3.right) * idealRoll;

            float pitchAmount = Mathf.Abs(Vector3.Dot(Physics.gravity.normalized, myRB.transform.up));
            float rollCorrMod = myFlightModel.PitchToRollCorrectionRamp.Evaluate(pitchAmount);

            float timeSinceLastInput = Time.time - Mathf.Max(lastRollInputTime, lastPitchInputTime);
            rollCorrMod *= myFlightModel.RollCorrectionCurve.Evaluate(timeSinceLastInput);

            myRB.transform.rotation = Quaternion.Slerp(myRB.transform.rotation, idealRoll, myFlightModel.RollCorrectionRate * rollCorrMod * Time.deltaTime);
        }

        private Quaternion CalculateNewOrientationForLateralCorrection()
        {
            Vector3 moveDirectionPlanar = Vector3.ProjectOnPlane(myRB.velocity, Vector3.up).normalized;
            Vector3 currentLookPlanar = Vector3.ProjectOnPlane(HeadsUpForward, Vector3.up).normalized;

            float moveToLookAngle = Vector3.Angle(moveDirectionPlanar, currentLookPlanar);
            Vector3 currentLookNormal = Vector3.Cross(currentLookPlanar, Vector3.up);
            bool turnRight = Vector3.Dot(currentLookNormal, moveDirectionPlanar) > 0.0f;
            if (turnRight) { moveToLookAngle *= -1; }

            Quaternion lateralOrientationCorrection = Quaternion.AngleAxis(moveToLookAngle, Vector3.up);
            Quaternion idealLateralOrientation = lateralOrientationCorrection * myRB.transform.rotation;

            //Reduce correction amount if at steep pitch
            float modifiedCorrectionRate = myFlightModel.YawCorrectionRate * (1 - NormalizedPitchAmount);

            //Reduce correction amount if moving slowly laterally
            float lateralSpeed = Vector3.ProjectOnPlane(myRB.velocity, Vector3.up).magnitude;
            modifiedCorrectionRate *= myFlightModel.LateralSpeedToYawCorrection.Evaluate(lateralSpeed);

            return Quaternion.Slerp(myRB.transform.rotation, idealLateralOrientation, modifiedCorrectionRate * Time.deltaTime);
        }

        // Update is called once per frame
        void Update()
        {
            InputUpdateRotations();
            UpdateIdealRollRotation();

            myRB.transform.rotation = CalculateNewOrientationForLateralCorrection();
            //myRB.velocity = myRB.velocity + (myRB.transform.forward * BaseSpeed * Time.deltaTime);
        }
        

        private void FixedUpdate()
        {
            Quaternion turnAmount = Quaternion.AngleAxis(CurrentRollAmount * myFlightModel.BankFromRollAmount, Vector3.up);
            myRB.velocity = turnAmount * myRB.velocity;



            float pitchForwardTargetAcceleration = PitchToRawGlideForce * myFlightModel.MaxGlideForce + RawDragFromPitch;


            //float glideForceAccelerationFromPitch = Mathf.Lerp(glideForceAccelerationFromPitch, pitchForwardTargetVelocity, Time.deltaTime);
            glideForceForward += pitchForwardTargetAcceleration;

            glideForceForward = Mathf.Clamp(glideForceForward, myFlightModel.MinGlideSpeed, myFlightModel.MaxGlideSpeed);

            Vector3 localVelocity = transform.InverseTransformDirection(myRB.velocity);

            localVelocity.z = glideForceForward;
            

            myRB.velocity = transform.TransformDirection(localVelocity);

            myRB.velocity += new Vector3(0, myFlightModel.PitchToGlideLift.Evaluate(PitchAmountRelativeToGravity) * myFlightModel.GlideLiftModifier, 0) ;
            //Add in lift based on orientation
            /*
            Vector3 liftForce = Vector3.up * (PitchAmountRelativeToGravity / myFlightModel.LiftAngle) * myFlightModel.GlideLiftModifier;

            myRB.AddForce(liftForce);
            */

            //myRB.velocity += (myRB.transform.forward * glideForceForward);
            /*
            float netGlideLiftModifier = 0;
            if (!UpsideDown)
            {
                float headForwardToMoveDirectionClampedDot = Mathf.Clamp(Vector3.Dot(HeadsUpForward, myRB.velocity.normalized), 0.0f, 1.0f);
                float speedInDirectionOfGlide = headForwardToMoveDirectionClampedDot * myRB.velocity.magnitude;

                float liftFromAngleOfAttackModifier = myFlightModel.angleOfAttackToGlideLift.Evaluate(AngleOfAttack);
                float liftFromPitchModifier = myFlightModel.pitchToGlideLift.Evaluate(PitchAmountRelativeToGravity);
                float liftFromSpeedModifier = myFlightModel.speedInDirectionOfHeadToGlideLift.Evaluate(speedInDirectionOfGlide);
                netGlideLiftModifier = liftFromAngleOfAttackModifier * liftFromPitchModifier * liftFromSpeedModifier;
            }

            Vector3 glideLiftForce = MaxGlideLift * netGlideLiftModifier;

            myRB.AddForce(glideLiftForce, ForceMode.Force);
            /*
            float bellyToMoveDirectionDot = Vector3.Dot(myRB.velocity.normalized, myRB.transform.forward);

            float netDragMod = myFlightModel.DragToVelocityNormalized.Evaluate(bellyToMoveDirectionDot);
            netDragMod *= bellyToMoveDirectionDot > 0f ? myFlightModel.wingBottomDragMultiplier : myFlightModel.wingTopDragMultiplier;

            float speedInDirOfWingDrag = Mathf.Abs(bellyToMoveDirectionDot) * myRB.velocity.magnitude;
            netDragMod *= myFlightModel.SpeedToDragRamp.Evaluate(speedInDirOfWingDrag);

            Vector3 dragForce = netDragMod * StandardExtremeAngleDragForce;
            myRB.AddForce(dragForce, ForceMode.Force);

            float dragModifier = myFlightModel.PitchToDragModifier.Evaluate(PitchAmountRelativeToGravity);
            myRB.drag = dragModifier * myFlightModel.MaxBodyDrag;*/
        }
    }


   
}

