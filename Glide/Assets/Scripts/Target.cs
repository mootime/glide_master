﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{

    public float value = 10.0f;
    
    // Update is called once per frame
    void Update()
    {
        
    }

  void OnTriggerEnter (Collider collide){

    if(collide.gameObject.tag == "Player"){

      collide.gameObject.GetComponentInChildren<PointsSystem>().points += value;
      Debug.Log(GameObject.Find("Trigger"));   

    }

    }

}
