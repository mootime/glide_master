﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Rendering.PostProcessing;


public class PointsSystem : MonoBehaviour
{

    public float points = 0.0f;
    Collider player_collide;
    public bool isdead = false;
    public bool hit_object = false;
    public bool fadein = false;
    public bool fadeout = false;

    //post processing
    public PostProcessVolume postpro;
    public Vignette pp_v;
    public float t = 0.0f;
    public float startt = 0.0f;
    public CharecterController CharecterController_script;
    public TextMeshProUGUI points_text;

    // Start is called before the first frame update
    void Start()
    {

        player_collide = GetComponent<Collider>();
        //postpro = GetComponent<PostProcessVolume>();
        postpro.profile.TryGetSettings<Vignette>(out pp_v);
        //CharecterController_script = GameObject.Find("Charecter").GetComponent<CharecterController>();
              
    }

    // Update is called once per frame
    void Update()
    {

        //set points ui
        points_text.text = (points.ToString("f0") + " PTS.");

        if(fadein == true){
            
            float current = (Time.time - startt) / 0.5f;

            pp_v.intensity.Override(Mathf.SmoothStep(0.0f, 0.6f, current));

            if(pp_v.intensity >= 0.6f){
                fadein = false;
            }

        }

        if(fadeout == true){

            float current = (Time.time - startt) / 5.1f;

            pp_v.intensity.Override(Mathf.SmoothStep(0.6f, 0.0f, current));

            if(pp_v.intensity <= 0.0f){
                fadeout = false;
            }

        }

    }

    void OnTriggerEnter (Collider collide){
    fadein = true;
    startt = Time.time;
    }

    void OnTriggerStay (Collider collide){

        //Add Points only if we have not hit something and it's not tagged with something we cant score points on
    if(hit_object == false && collide.tag != "Stumble" && collide.tag != "NoPoints" && collide.tag != "Target" && isdead == false){
            points += 0.1f;


            fadeout = false;

        }
    }


  void OnTriggerExit (Collider collide){

        fadeout = true;

        startt = Time.time;
    }

    public void HitCooldown (){

            hit_object = false;

    }

}
