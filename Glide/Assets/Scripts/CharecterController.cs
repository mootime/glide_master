﻿//Sorry its a mess in here.... -Bailey

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharecterController : MonoBehaviour
{
    //speed not really used (display only)
    public float speed = 12.5f;
    //Defualts 30 max 18 base 22.5
    public float sensitivity = 0.05f;

    public float max_speed = 50.0f;
    public float min_speed = 18.5f;
    public float base_speed = 22.5f;

    public float percentage;

    public Rigidbody rb;

    private Vector3 rot;

    public Vector3 localV;

    public bool startslowcount = false;

    public Vector3 vstore;

    public float stime;

    public float nextime = 0.0f;

    public PointsSystem PointsSystem_script;

    public ParticleSystem SlipStream;

    //animation vars
    public Animator c_animator;

    //sound vars
    public AudioSource cc_audiosource;

    // Start is called before the first frame update
    void Start()
    {
       rb = GetComponent<Rigidbody>();
       PointsSystem_script = GameObject.Find("Trigger").GetComponent<PointsSystem>();

       rot = transform.eulerAngles;

        //Turn off mouse
        Cursor.lockState = CursorLockMode.Locked;

        c_animator = GameObject.Find("MainCharacter_Low").GetComponent<Animator>();
        cc_audiosource = GetComponent<AudioSource>();

        SlipStream = GameObject.Find("SlipStream").GetComponent<ParticleSystem>();
     
    }

    // Update is called once per frame
    void Update()
    {

    Cursor.visible = false;

    //clamp left and right so player does not go too far
        //Vector3 pos = transform.position;
        //pos.x = Mathf.Clamp(transform.position.x, -20, 20);
        //transform.position = pos;

        //Get player input and clamp said values
        rot.x += 20 * Input.GetAxis("Mouse Y") * sensitivity;
        rot.x = Mathf.Clamp(rot.x, -45, 75);

        rot.y += 20 * Input.GetAxis("Mouse X") * sensitivity;
        //rot.y = Mathf.Clamp(rot.y, -80, 80);

        //Same but Controller
        rot.x += 20 * Input.GetAxis("Controller Y") * sensitivity;
        rot.y += 20 * Input.GetAxis("Controller X") * sensitivity;

        //Same but WASD
        rot.x += 20 * Input.GetAxis("Vertical") * sensitivity;
        rot.y += 20 * Input.GetAxis("Horizontal") * sensitivity;

    //Debug.Log( Input.GetAxis("PS4_stick_DEBUG") );

    //write to animation parameters

    if(c_animator != null){

    c_animator.SetFloat("VertAngle", transform.rotation.eulerAngles.x);
    c_animator.SetFloat("HorAngle", transform.rotation.eulerAngles.y);


     if(Input.GetAxis("Mouse X") > 0.5 || Input.GetAxis("Controller X") > 0.5 || Input.GetAxis("Horizontal") > 0.5){

            c_animator.SetBool("BRight", true);
            c_animator.SetBool("BLeft", false);

        }

      if(Input.GetAxis("Mouse X") < -0.5 ||Input.GetAxis("Controller Y") < -0.5 || Input.GetAxis("Vertical") < -0.5){

            c_animator.SetBool("BLeft", true);
            c_animator.SetBool("BRight", false);

        }

      if(Input.GetAxis("Mouse X") == 0 && Input.GetAxis("Controller Y") == 0 && Input.GetAxis("Vertical") == 0){

            c_animator.SetBool("BLeft", false);
            c_animator.SetBool("BRight", false);

        }


        //new input based system
      //c_animator.SetFloat("Vertical", Input.GetAxis("Mouse X"));
      //c_animator.SetFloat("Horizontal", Input.GetAxis("Mouse Y"));

    }

    //windsound

    cc_audiosource.volume = (((speed - 16.5f)/35.5f)*2.0f);
    //Debug.Log(((speed - 16.5f)/35.5f)*2.0f);

    //slip stream
    var main = SlipStream.emission;
    main.rate = scale(24.7f, 36f, 0f, 22f, speed);
    //Debug.Log(scale(18.1f, 36.5f, 0f, 22f, speed));


        //take input and actually rotate
        transform.rotation = Quaternion.Euler(rot);

        //increases or decreases drag depending on this calculation
        //calculates how much of the max angle the player is at and give us a percentage
        percentage = rot.x / 75;

        //drag calculation d=6 
        float mod_drag = (percentage * -2) + 3;

        //spped min - max (d = 12.8/12.5)
        float mod_speed = percentage * (max_speed - min_speed) + base_speed;

        //Debug.Log(((45/45) * -2)+3);
        //set drag
        rb.drag = mod_drag;
        //moves charecter foward

        //pull up
        if(percentage < 0.0f){

            if(startslowcount == false){
                startslowcount = true;
                nextime = Time.time + 0.1f;
            }

        if(Time.time > nextime){
        //localV = lerper(vstore, transform.InverseTransformDirection(new Vector3 (0, -45,0)), stime, Vector3.Distance(vstore, transform.InverseTransformDirection(new Vector3 (0, -45,0)) ) );
                //Vector3 side = transform.TransformDirection(transform.forward);
                //localV.x = side.x*-20;
                //Vector3 noup = transform.TransformDirection(localV);
                //noup.y = -6;
                //rb.velocity = localV;
                //Debug.DrawRay(transform.position, localV);
                localV = transform.forward;
                rb.velocity = new Vector3 (localV.x*mod_speed, -6, localV.z*mod_speed);
                Debug.DrawRay(transform.position, rb.velocity, Color.yellow);

            }
            else{
                transform.TransformDirection(rb.velocity);
                rb.velocity = transform.TransformDirection(localV);
                //Debug.DrawRay(transform.position, transform.TransformDirection(localV));
                vstore = transform.TransformDirection(localV);
                stime = Time.time;
            }
        }

        else{

            // zero out 
            localV.x = 0;
            localV.y = 0;
            transform.InverseTransformDirection(rb.velocity);
            rb.velocity = transform.TransformDirection(localV);
            localV.z = mod_speed;
            //Debug.DrawRay(transform.position, transform.TransformDirection(localV));
            startslowcount = false;

        }

        //Debug.DrawRay(transform.position, localV*14, Color.yellow);

        //oldspot
        //localV.z = mod_speed;

        //Debug.Log(percentage);

        //set speedo
        speed = mod_speed;

        if(PointsSystem_script.hit_object == true){
            rb.velocity = rb.velocity * 0.3f;
            rb.drag = 1f;
        }
          
    }

    public Vector3 lerper (Vector3 vfrom, Vector3 vto, float startime, float distance){

        float curdist = (Time.time - startime) * 2.0f;

        float frac = curdist / distance;

        vfrom = Vector3.Lerp(vfrom ,vto, frac);
        vfrom.z = localV.z; 
        Debug.Log(vfrom);
        return vfrom;

    }

    //did we hit something? If so lets temp disable the collection of points
    void OnCollisionEnter (Collision collide){

        Debug.Log(collide.gameObject.name);
        PointsSystem_script.hit_object = true;

        KillPlayer();

    }

    void OnCollisionStay (Collision collide){

      PointsSystem_script.hit_object = true;  

    }

    void OnCollisionExit (Collision collide){

      PointsSystem_script.Invoke("HitCooldown", 2.1f);  

    }

    public void KillPlayer (){

        PointsSystem_script.hit_object = true;

        PointsSystem_script.isdead = true;  
        GameObject.Find("MainCharacter_Low").SetActive(false);
        GameObject.Find("MenuManager/GameOverManager/Game Over").SetActive(true);
        Instantiate(Resources.Load("prefab/Character_Ragdoll"), transform.position, transform.rotation);
        this.GetComponent<CharecterController>().enabled = false;
        this.GetComponent<Rigidbody>().isKinematic = true;
        //Time.timeScale = 0.0f;

    }

  public float scale(float OldMin, float OldMax, float NewMin, float NewMax, float OldValue){
 
    float OldRange = (OldMax - OldMin);
    float NewRange = (NewMax - NewMin);
    float NewValue = (((OldValue - OldMin) * NewRange) / OldRange) + NewMin;
 
    return(NewValue);

}

}
