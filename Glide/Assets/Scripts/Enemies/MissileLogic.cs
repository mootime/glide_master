﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileLogic : MonoBehaviour
{

    public Rigidbody rb;
    public float speed = 10.0f;
    public float boom_distence = 75.0f;
    public string target = "/Character";

    // Start is called before the first frame update
    void Start()
    {

        rb = GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {

        transform.LookAt(GameObject.Find(target).transform);
        rb.velocity = this.transform.forward * speed;

    if(Vector3.Distance(this.transform.position, GameObject.Find(target).transform.position) < boom_distence && target == "/Character"){

            //also destroy(this) works as a good cutoff if you want it to sail past :)
            Instantiate(Resources.Load("prefab/Particle_Explosion"), transform.position, transform.rotation);
            Instantiate(Resources.Load("prefab/FX_AudioShot"), transform.position, transform.rotation);
            Debug.Log(Resources.Load("prefab/FX_AudioShot"));
            Destroy(this.gameObject);

        }

    }

  void OnCollisionEnter (Collision collide){

            Instantiate(Resources.Load("prefab/Particle_Explosion"), transform.position, transform.rotation);
            Instantiate(Resources.Load("prefab/FX_AudioShot"), transform.position, transform.rotation);
            Destroy(this.gameObject);

  }

}
