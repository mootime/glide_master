﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBasic : MonoBehaviour
{

    public Vector3 direction;
    public float speed = 20.0f;
    public float active_distence = 500.0f;
    public float destroy_distence = 850.0f;
    private bool been_active = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {

    if(Vector3.Distance(this.transform.position, GameObject.Find("/Character").transform.position) < active_distence){
            been_active = true;
      //transform.Translate(direction * speed * Time.smoothDeltaTime, Space.World);
      //transform.position = (direction * speed * Time.smoothDeltaTime);
      GetComponent<Rigidbody>().velocity = direction * speed;

        }

    if(Vector3.Distance(this.transform.position, GameObject.Find("/Character").transform.position) > destroy_distence && been_active == true){

    Destroy(this.gameObject);

    }
              
    }
}
