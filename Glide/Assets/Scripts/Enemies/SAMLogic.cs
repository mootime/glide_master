﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SAMLogic : MonoBehaviour
{

    AudioSource muPlayer;

    public float active_distence = 500.0f;
    public bool fired = false;

    public string target = "/Character";

    // Start is called before the first frame update
    void Start()
    {
        muPlayer = GetComponent<AudioSource>();  
    }

    // Update is called once per frame
    void Update()
    {

        //turret rotation
    if(GameObject.Find(target) != null){
        transform.LookAt( new Vector3 ( GameObject.Find(target).transform.position.x, transform.position.y, GameObject.Find(target).transform.position.z ) );
    }

        //missle launch
    if(Vector3.Distance(this.transform.position, GameObject.Find("/Character").transform.position) < active_distence && fired == false){
            fired = true;
            Instantiate(Resources.Load("prefab/Particle_Launch"), new Vector3(transform.position.x, transform.position.y + 6.8f, transform.position.z), transform.rotation);
            muPlayer.clip = Resources.Load<AudioClip>("FX/Missile_Launch");
            muPlayer.PlayOneShot(Resources.Load<AudioClip>("FX/Missile_Launch"), 0.2f);
            GameObject missile = GameObject.Instantiate(Resources.Load("prefab/sam_missile"), new Vector3(transform.position.x, transform.position.y + 6.8f, transform.position.z), transform.rotation) as GameObject;
            missile.GetComponent<MissileLogic>().target = target;
         }

    }
}
